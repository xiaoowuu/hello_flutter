import 'package:flutter/material.dart';
import 'package:hello_flutter/DetailPage.dart';
import 'package:hello_flutter/Planet.dart';
import 'package:hello_flutter/PlanetSummary.dart';

class HomePageBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Expanded(
        child: new ListView.builder(
      itemBuilder: (context, index) => new GestureDetector(
          child: new PlanetSummary(planets[index]),
          onTap: () => Navigator.push(
              context,
              new PageRouteBuilder(
                  pageBuilder: (_, __, ___) =>
                      new DetailPage(planets[index])))),
      itemCount: planets.length,
      padding: EdgeInsets.symmetric(vertical: 16.0),
    ));
  }
}
