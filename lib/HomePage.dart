import 'package:flutter/material.dart';
import 'package:hello_flutter/GradientAppBar.dart';
import 'package:hello_flutter/HomePageBody.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: new Column(
      children: <Widget>[
        new GradientAppBar("Hello"),
        new HomePageBody(),
      ],
    ));
  }
}
